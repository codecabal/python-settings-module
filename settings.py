from os.path import expanduser
import platform
import logging
import os.path
import sys
import os

logging.basicConfig(
    format="%(asctime)s %(levelname)s %(name)s:> %(message)s", level=logging.DEBUG)
logger = logging.getLogger(__name__)


class Settings():

    """
        Used to store user settings in os specific user directory(for Win/Linux/Android).
    """
    company_name = None
    app_name = None
    file_path = None
    file_name = None

    def init(self, ncompany_name, napp_name, nfile_name, nfile_path=""):
        """
            company_name - Company name, defines exterior folder
            app_name - The folder in which the settings file will be placed
            file_name - The name of the settings file
            file_path - Path to the directory where the settings file should be placed if specifed,
                            selects os appropriate directory if not specifed.
            Remark: The idea is that one company may have multiple applications
            with settings that can be contained in one company folder,
            each having it's own application folder to put the setting file.
            Currently tested only on windows, but should work on linux, mac, bsd etc.
        """
        logger.info("Constructor.")
        Settings.company_name = ncompany_name
        Settings.app_name = napp_name
        Settings.file_path = nfile_path
        Settings.file_name = nfile_name

    def fetch_settings_folder_path(self):
        """
            Returns settings folder path if possible, current dirrectory otherwise.
        """
        logger.info("Fetching settings folder path.")
        if not Settings.file_path == "":
            return Settings.file_path
        system = platform.system().upper()
        if system == "WINDOWS":
            path = expanduser("~") + os.sep + "AppData" + os.sep + "Local" + \
                os.sep + Settings.company_name + \
                os.sep + Settings.app_name + os.sep
            if os.path.exists(path):
                logger.info("Returning windows settings folder path.")
                return path
            else:
                logger.info("Creating settings path.")
                try:
                    os.makedirs(path, 0o770)
                    logger.info("Settings path created, returning.")
                    return path
                except os.error:
                    logger.error(
                        "Could not fetch settings folder and failed to create it. Returning in shame!" + str(sys.exc_info()[0]))
                    return "." + os.sep
        elif system == "LINUX":
            path = expanduser("~") + os.sep + ".config" + os.sep + \
                Settings.company_name + os.sep + Settings.app_name + os.sep
            if os.path.exists(path):
                logger.info("Returning linux settings folder path.")
                return path
            else:
                logger.info("Creating settings path.")
                try:
                    os.makedirs(path, 0o770)
                    logger.info("Settings path created, returning.")
                    return path
                except os.error:
                    logger.error(
                        "Could not fetch settings folder and failed to create it. Returning in shame!" + str(sys.exc_info()[0]))
                    return "." + os.sep
        elif system == "JAVA":
            path = expanduser("~") + os.sep + ".config" + os.sep + \
                Settings.company_name + os.sep + Settings.app_name + os.sep
            if os.path.exists(path):
                logger.info("Returning java settings folder path.")
                return path
            else:
                logger.info("Creating settings path.")
                try:
                    os.makedirs(path, 0o770)
                    logger.info("Settings path created, returning.")
                    return path
                except os.error:
                    logger.error(
                        "Could not fetch settings folder and failed to create it. Returning in shame!" + str(sys.exc_info()[0]))
                    return "." + os.sep
        else:
            logger.error(
                "Unable to determine system type: " + system + ", could not fetch settings folder path.")
            return "." + os.sep

    def fetch_settings_file_path(self):
        """
            Returns settings file path.
        """
        logger.info("Returning file path.")
        return self.fetch_settings_folder_path() + Settings.file_name

    def update_value(self, key, value):
        logger.info("Updating settings value.")
        file = self.fetch_settings_file_path()
        if not os.path.isfile(file):
            logger.info("Settings file does not exist, creating it now.")
            with open(file, "w") as f:
                pass
        with open(file, "r+") as f:
            with open(file + "tmp", "w") as ftmp:
                keys = []
                for line in f:
                    line = line.strip().split("=")
                    if len(line) != 2:
                        continue
                    line = [line[0].strip(), line[1].strip()]
                    aux = []
                    if line[0] == key:
                        aux = [line[0], value]
                    else:
                        aux = line
                    if not line[0] in keys:
                        ftmp.write(aux[0] + "=" + aux[1] + os.linesep)
                        keys = keys + [line[0]]
                if not key in keys:
                    ftmp.write(key + "=" + value + os.linesep)
        if os.path.isfile(file):
            if os.path.isfile(file + "tmp"):
                try:
                    os.remove(file)
                    os.rename(file + "tmp", file)
                except:
                    logger.error(
                        "Sorry some error occured, could not finish updating the setting.")
                    return False
        logger.info("Settings value updated.")
        return True

    def fetch_value(self, key):
        """
            Returns the value of the specified setting if present, None otherwise.
        """
        logger.info("Returning value for key " + key)
        path = self.fetch_settings_file_path()
        if os.path.exists(path):
            with open(path, "r") as f:
                for line in f:
                    line = line.strip().split("=")
                    if(len(line) != 2):
                        continue
                    line = [line[0].strip(), line[1].strip()]
                    logger.info("Returning value " + line[1])
                    return line[1]
        else:
            logger.warning(
                "Could not find settings file returning None, sorry.")
            return None
