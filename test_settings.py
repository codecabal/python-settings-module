import os
import platform
import unittest
from settings import *


class TestSettings(unittest.TestCase):

    def test_init(self):
        Settings().init("A", "B", "nope.txt")
        self.assertTrue(Settings.company_name == "A")
        self.assertTrue(Settings.app_name == "B")
        self.assertTrue(Settings.file_name == "nope.txt")
        self.assertTrue(Settings.file_path == "")
        Settings().init("C", "D", "nopex.txt", "c:\\C\\D\\")
        self.assertTrue(Settings.company_name == "C")
        self.assertTrue(Settings.app_name == "D")
        self.assertTrue(Settings.file_name == "nopex.txt")
        self.assertTrue(Settings.file_path == "c:\\C\\D\\")

    def test_fetch_settings_folder_path(self):
        a = Settings()
        a.init("A", "B", "s.txt", "c:\\")
        self.assertTrue(a.fetch_settings_folder_path() == "c:\\")
        a.init("A", "B", "s.txt")
        plat = platform.system().upper()
        path = ""
        if plat == "WINDOWS":
            path = expanduser("~") + os.sep + "AppData" + os.sep + "Local" + \
                os.sep + Settings.company_name + \
                os.sep + Settings.app_name + os.sep
            self.assertTrue(path == a.fetch_settings_folder_path())
        elif plat == "LINUX":
            path = expanduser("~") + os.sep + ".config" + os.sep + \
                Settings.company_name + os.sep + Settings.app_name + os.sep
            self.assertTrue(path == a.fetch_settings_folder_path()
                            or a.fetch_settings_folder_path() == "." + os.sep)
        elif plat == "JAVA":
            path = expanduser("~") + os.sep + ".config" + os.sep + \
                Settings.company_name + os.sep + Settings.app_name + os.sep
            self.assertTrue(
                path == a.fetch_settings_folder_path() or path == "." + sep)
        else:
            self.assertTrue(plat == "." + os.sep)

    def test_settings_file_path(self):
        a = Settings()
        a.init("A", "B", "s.txt", "c:\\")
        self.assertTrue(
            a.fetch_settings_file_path() == a.fetch_settings_folder_path() + Settings.file_name)
        a.init("A", "B", "s.txt")
        self.assertTrue(
            a.fetch_settings_file_path() == a.fetch_settings_folder_path() + Settings.file_name)

    def test_update_value(self):
        a = Settings()
        a.init("A", "B", "s.txt")
        self.assertTrue(a.update_value("a", "B"))
        os.remove(a.fetch_settings_file_path())

    def test_fetch_value(self):
        a = Settings()
        a.init("A", "B", "s.txt")
        self.assertTrue(a.fetch_value("x") == None)
        a.update_value("x", "v")
        self.assertTrue(a.fetch_value("x") == "v")

if __name__ == "__main__":
    unittest.main()
